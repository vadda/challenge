package com.db.awmd.challenge;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountOverdraftNotAllowedException;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.exception.NoSuchAccountException;
import com.db.awmd.challenge.exception.TransferAmountShouldBePositive;
import com.db.awmd.challenge.repository.AccountsRepository;
import com.db.awmd.challenge.repository.AccountsRepositoryInMemory;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.service.NotificationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountsServiceTransferUnitTest {

    @InjectMocks
    private AccountsService service;

    @Spy
    private AccountsRepository repository = new AccountsRepositoryInMemory();

    @Mock
    private NotificationService notificationService;

    @Before
    public void init() {
        doNothing().when(notificationService).notifyAboutTransfer(any(Account.class), anyString());
    }

    @After
    public void tearDown() {
        repository.clearAccounts();
    }

    @Test(expected = NoSuchAccountException.class)
    public void shouldThrowExceptionWhenTransferToNonExistingAccount() {
        final Account source = new Account("1", BigDecimal.TEN);
        service.createAccount(source);
        service.transfer("1", "2", BigDecimal.ONE);
    }

    @Test(expected = NoSuchAccountException.class)
    public void shouldThrowExceptionWhenTransferFromNonExistingAccount() {
        final Account target = new Account("2", BigDecimal.TEN);
        service.createAccount(target);
        service.transfer("1", "2", BigDecimal.ONE);
    }

    @Test(expected = TransferAmountShouldBePositive.class)
    public void shouldThrowExceptionWhenTransferAmountIsNotPositive() {
        final Account source = new Account("1", BigDecimal.TEN);
        service.createAccount(source);
        final Account target = new Account("2", BigDecimal.TEN);
        service.createAccount(target);
        service.transfer("1", "2", BigDecimal.ZERO);
    }

    @Test(expected = AccountOverdraftNotAllowedException.class)
    public void transferShouldNotAllowOverdraft() {
        final Account source = new Account("1", BigDecimal.TEN);
        service.createAccount(source);
        final Account target = new Account("2", BigDecimal.TEN);
        service.createAccount(target);
        service.transfer("1", "2", BigDecimal.valueOf(11L));
    }

    @Test
    public void shouldTransferFunds() {
        final BigDecimal initialFunds = BigDecimal.TEN;
        final BigDecimal amount = BigDecimal.valueOf(3L);
        final Account source = new Account("1", initialFunds);
        service.createAccount(source);
        final Account target = new Account("2", initialFunds);
        service.createAccount(target);
        service.transfer("1", "2", amount);
        assertThat(source.getBalance()).isEqualTo(initialFunds.subtract(amount));
        assertThat(target.getBalance()).isEqualTo(initialFunds.add(amount));
    }

    @Test
    public void shouldSendNotifications() {
        final BigDecimal initialFunds = BigDecimal.TEN;
        final BigDecimal amount = BigDecimal.valueOf(3L);
        final Account source = new Account("1", initialFunds);
        service.createAccount(source);
        final Account target = new Account("2", initialFunds);
        service.createAccount(target);
        service.transfer("1", "2", amount);
        verify(notificationService, times(1))
                .notifyAboutTransfer(source, String.format("funds transferred to %s, amount = %s", target.getAccountId(), amount));
        verify(notificationService, times(1))
                .notifyAboutTransfer(target, String.format("funds transferred from %s, amount = %s", source.getAccountId(), amount));

    }
}
