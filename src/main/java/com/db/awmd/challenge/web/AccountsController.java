package com.db.awmd.challenge.web;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountOverdraftNotAllowedException;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.exception.NoSuchAccountException;
import com.db.awmd.challenge.service.AccountsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/accounts")
@Slf4j
public class AccountsController {

    private final AccountsService accountsService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createAccount(@RequestBody @Valid Account account) {
        log.info("Creating account {}", account);

        try {
            this.accountsService.createAccount(account);
        } catch (DuplicateAccountIdException daie) {
            return new ResponseEntity<>(daie.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(path = "/{accountId}")
    public Account getAccount(@PathVariable String accountId) {
        log.info("Retrieving account for id {}", accountId);
        return this.accountsService.getAccount(accountId);
    }

    @GetMapping(path = "/transfer")
    public ResponseEntity<String> transfer(
            @RequestParam("source") @NotNull final String sourceAccountId,
            @RequestParam("target") @NotNull final String targetAccountId,
            @RequestParam("amount") @Min(value = 0, message = "transfer amount must be positive.") final BigDecimal amount) {
        log.info("transferring funds from {} to {}, amount = {}", sourceAccountId, targetAccountId, amount);

        try {
            if (sourceAccountId.equals(targetAccountId)) {
                throw new IllegalStateException("source and target accounts must not match");
            }
            this.accountsService.transfer(sourceAccountId, targetAccountId, amount);
        } catch (AccountOverdraftNotAllowedException | NoSuchAccountException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
