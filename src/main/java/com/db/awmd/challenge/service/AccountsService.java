package com.db.awmd.challenge.service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountOverdraftNotAllowedException;
import com.db.awmd.challenge.exception.NoSuchAccountException;
import com.db.awmd.challenge.repository.AccountsRepository;
import com.db.awmd.challenge.exception.TransferAmountShouldBePositive;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountsService {

    @Getter
    private final AccountsRepository accountsRepository;

    private final NotificationService notificationService;

    public void createAccount(Account account) {
        this.accountsRepository.createAccount(account);
    }

    public Account getAccount(String accountId) {
        return this.accountsRepository.getAccount(accountId);
    }

    public void transfer(final String sourceAccountId, final String targetAccountId, final BigDecimal amount) throws
            AccountOverdraftNotAllowedException,
            TransferAmountShouldBePositive,
            NoSuchAccountException {
        try {
            final Account sourceAccount = Optional.ofNullable(getAccount(sourceAccountId))
                    .orElseThrow(() -> new NoSuchAccountException(sourceAccountId));
            final Account targetAccount = Optional.ofNullable(getAccount(targetAccountId))
                    .orElseThrow(() -> new NoSuchAccountException(targetAccountId));
            if (amount.compareTo(BigDecimal.ZERO) <= 0) {
                throw new TransferAmountShouldBePositive(amount);
            }
            // account ids are not equal so it's safe not to check on equality
            final boolean sourceIsFirst = sourceAccountId.compareTo(targetAccountId) > 0;
            final Account firstAccountToLock = sourceIsFirst ? sourceAccount : targetAccount;
            final Account secondAccountToLock = sourceIsFirst ? targetAccount : sourceAccount;
            // synchronization is done on account ids which must be interned prior to use
            final String firstAccountId = firstAccountToLock.getAccountId().intern();
            final String secondAccountId = secondAccountToLock.getAccountId().intern();

            // synchronization order must be deterministic in order to avoid deadlock
            synchronized (firstAccountId) {
                // re-read account after placing a lock as it may have changed
                if (sourceIsFirst) {
                    refreshAccountBalance(sourceAccount);
                    ensureEnoughFunds(sourceAccount, amount);
                } else {
                    refreshAccountBalance(targetAccount);
                }
                synchronized (secondAccountId) {
                    if (sourceIsFirst) {
                        refreshAccountBalance(targetAccount);
                    } else {
                        refreshAccountBalance(sourceAccount);
                        ensureEnoughFunds(sourceAccount, amount);
                    }
                    doTransfer(sourceAccount, targetAccount, amount);
                    // persist ?
                }
            }
            // accounts ids are final so it's safe to move notification out of synchronized block
            // there is no requirement to do it in a transactional way
            notificationService.notifyAboutTransfer(sourceAccount, String.format("funds transferred to %s, amount = %s", targetAccount.getAccountId(), amount));
            notificationService.notifyAboutTransfer(targetAccount, String.format("funds transferred from %s, amount = %s", sourceAccount.getAccountId(), amount));
        } catch (Exception e) {
            log.error("error during transferring funds", e);
            throw e;
        }
    }

    private void refreshAccountBalance(final Account account) {
        final Account a = accountsRepository.getAccount(account.getAccountId());
        if (!a.getBalance().equals(account.getBalance())) {
            account.setBalance(a.getBalance());
        }
    }

    private void doTransfer(Account sourceAccount, Account targetAccount, BigDecimal amount) {
        final BigDecimal sourceAccountBalance = sourceAccount.getBalance();
        final BigDecimal targetAccountBalance = targetAccount.getBalance();
        try {
            sourceAccount.setBalance(sourceAccount.getBalance().subtract(amount));
            targetAccount.setBalance(targetAccount.getBalance().add(amount));
        } catch (Throwable e) {
            log.error("error during funds transfer", e);
            // rolling back changes in case of any error
            sourceAccount.setBalance(sourceAccountBalance);
            targetAccount.setBalance(targetAccountBalance);
            throw e;
        }
    }

    private void ensureEnoughFunds(Account account, BigDecimal amount) {
        if (account.getBalance().compareTo(amount) < 0) {
            throw new AccountOverdraftNotAllowedException("insufficient funds in account " + account.getAccountId());
        }
    }
}
