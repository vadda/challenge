package com.db.awmd.challenge.exception;

public class AccountOverdraftNotAllowedException extends RuntimeException {
    public AccountOverdraftNotAllowedException(String message) {
        super(message);
    }
}
