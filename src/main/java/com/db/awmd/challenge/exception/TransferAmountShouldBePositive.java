package com.db.awmd.challenge.exception;

import java.math.BigDecimal;

public class TransferAmountShouldBePositive extends RuntimeException {
    public TransferAmountShouldBePositive(BigDecimal amount) {
        super("Transfer amount should be positive number, actual = " + amount);
    }
}
