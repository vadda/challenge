package com.db.awmd.challenge.exception;

public class NoSuchAccountException extends RuntimeException {
    public NoSuchAccountException(String accountId) {
        super("account " + accountId + " does not exists");
    }
}
